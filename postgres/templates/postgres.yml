apiVersion: acid.zalan.do/v1
kind: postgresql
metadata:
  name: "{{ include "postgres.fullname" . }}"
  labels:
    {{- include "postgres.labels" . | nindent 4 }}
spec:
  teamId: "{{ include "postgres.fullname" . }}"
  volume:
    size: "{{ .Values.volumeSize }}"
  numberOfInstances: 2
  users:
  {{- range $user := .Values.users }}
    {{- $user | nindent 4 }}: []
  {{- end }}
  databases:
  {{- range $database := .Values.databases }}
    {{- $database.name | nindent 4 }}: {{ $database.owner }}
  {{- end }}
  postgresql:
    version: "14"
  sidecars:
    - name: "exporter"
      image: quay.io/prometheuscommunity/postgres-exporter:v0.11.1
      ports:
        - name: exporter
          containerPort: 9187
          protocol: TCP
      resources:
        limits:
          cpu: 500m
          memory: 256M
        requests:
          cpu: 100m
          memory: 200M
      env:
        - name: DATA_SOURCE_URI
          value: "{{ include "postgres.fullname" . }}?sslmode=require"
        - name: DATA_SOURCE_USER
          valueFrom:
            secretKeyRef:
              name: postgres.{{ include "postgres.fullname" . }}.credentials.postgresql.acid.zalan.do
              key: username
        - name: DATA_SOURCE_PASS
          valueFrom:
            secretKeyRef:
              name: postgres.{{ include "postgres.fullname" . }}.credentials.postgresql.acid.zalan.do
              key: password
        - name: PG_EXPORTER_AUTO_DISCOVER_DATABASES
          value: "true"
---
apiVersion: v1
kind: Service
metadata:
  name: {{ include "postgres.fullname" . }}-metrics-master
  labels:
    application: spilo
    spilo-role: master
spec:
  type: ClusterIP
  ports:
    - name: exporter
      port: 9187
      targetPort: exporter
  selector:
    application: spilo
    cluster-name: {{ include "postgres.fullname" . }}
    spilo-role: master
---
apiVersion: v1
kind: Service
metadata:
  name: {{ include "postgres.fullname" . }}-metrics-replica
  labels:
    application: spilo
    spilo-role: replica
spec:
  type: ClusterIP
  ports:
    - name: exporter
      port: 9187
      targetPort: exporter
  selector:
    application: spilo
    cluster-name: {{ include "postgres.fullname" . }}
    spilo-role: replica
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: {{ include "postgres.fullname" . }}-master
  labels:
    application: spilo
    spilo-role: master
spec:
  endpoints:
    - port: exporter
      interval: 15s
      scrapeTimeout: 10s
  selector:
    matchLabels:
      application: spilo
      spilo-role: master
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: {{ include "postgres.fullname" . }}-replica
  labels:
    application: spilo
    spilo-role: replica
spec:
  endpoints:
    - port: exporter
      interval: 15s
      scrapeTimeout: 10s
  selector:
    matchLabels:
      application: spilo
      spilo-role: replica
